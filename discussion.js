/*
tables to collection
rows to document
columns to fields
*/


// CRUD Operations

// Insert Documents (CREATE)
/*
	
	Syntax:
		Insert One Document
			db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB"
			})

		Insert Many Document
			db.collectionName.insertMany([
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				},
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				}
			])
*/

db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@mail.com",
	"department": "none"
})

db.users.insertMany([
		{
			"firstName": "Stephen",
			"lastName": "Hawking",
			"age": 76,
			"email": "stephen@mail.com",
			"department": "none"
		},
		{
			"firstName": "Neil",
			"lastName": "Armstrong",
			"age": 82,
			"email": "neil@mail.com",
			"department": "none"
		}
	])

db.courses.insertMany([
	{
		"name": "Javascript 101",
		"price": 5000,
		"description": "Indroduction to Javascript",
		"isActive": true
	},
	{
		"name": "HTML 101",
		"price": 2000,
		"description": "Indroduction to HTML",
		"isActive": true
	},
	{
		"name": "CSS 101",
		"price": 2500,
		"description": "Indroduction to CSS",
		"isActive": false
	}
])

//Find Document (Read) 
/*

	Syntax:
		db.collection.find() - this will retrieve all our documents
		
		db.collection.find({"criteria": "value"}) - this will retrieve all our documents that will match will our criteria.

		db.collectionName.findOne({}) will return the first documents in our collection

		db.collectionName.findOne({"criteria":"value"}) - will return the first document in our collection that will match our criteria.
*/

db.users.find();

db.users.find({
	"firstName": "Jane",
})

db.users.findOne()

db.users.findOne({
	"department": "none"
})

// Updating Documents (Update)
/*
	Syntax: 
		db.collectionName.updateOne (
			{
				"criteria": "value"
			},
			{
				$set: {
					"fieldTobeUpdate": "updatedValue"
				}
			}
		)

		db.collectionName.updateMany (
			{
				"criteria": "value"
			},
			{
				$set: {
					"fieldTobeUpdate": "updatedValue"
				}
			}
		)
*/

db.users.insertOne(
	{
		"firstName": "Test",
		"lastName": "Test",
		"age": 0,
		"email": "Test@mail.com",
		"department": "none"
	}
)

// Updating Multiple Documents
db.users.updateMany( 
	{
		"department": "none"
	},
	{
		$set: {
			"department"	: "HR"
		}
	}
)


db.users.updateOne(
	{
		"firstName": "Jane",
		"lastName": "Doe",	
	},
	{
		$set: {
			"department": "operation"
		}
	}
)


db.users.updateOne(
	{
		"firstName": "Bill"
	},
	{
		$unset: {
			"status": "active"
		}
	}
)

db.courses.updateOne(
	{
		"name": "HTML 101"
	},
	{
		$set: {
			"isActive": false,
		}
	}
)

db.courses.updateMany(
	{},
	{
		$set: {
			"enrollees": 10
		}
	}
)


// Deleting Documents (DELETE)

/*
	Syntax:
		db.collectionName.deleteOne({"criteria": "value"})
*/

db.users.insertOne({
	"firstName": "Test"
});

db.users.deleteOne({
	"firstName": "Test"
});

db.users.deleteMany({
	"dept": "HR"
});

db.users.deleteMany({});